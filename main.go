package main

import (
	"fmt"

	"bitbucket.org/sergsoloviev/monitoring/disk"
)

func main() {

	usage := disk.Usage("/")
	fmt.Printf("All: %.2f GB\n", float64(usage.All)/float64(disk.GB))
	fmt.Printf("Used: %.2f GB\n", float64(usage.Used)/float64(disk.GB))
	fmt.Printf("Free: %.2f GB\n", float64(usage.Free)/float64(disk.GB))
	fmt.Printf("All: %d GB\n", usage.All)
	fmt.Printf("Used: %d GB\n", usage.Used)
	fmt.Printf("Free: %d GB\n", usage.Free)

	//mem()
}
